﻿using System;

namespace ConsoleApplication1
{
	public class Program
	{
		static void Main(string[] args)
		{
			var c = new Controller();
			c.EngageMachinery();
			c.ApplyStatus(StatusList.Damaged.ToString());
			c.DisengageMachinery();

			/***** test two *****/
			var b = new Boiler();
			var h = new Heater();

			Controller.EngageSystem system;
			system = b.Engage;
			system += h.Engage;

			Controller.SetStatus status;
			status = b.SetStatus;
			status += h.SetStatus;

			new Controller().EngageAndSetStatus(system, status);
		}
	}

	public enum StatusList
	{
		Ready = 1,
		Broken,
		Damaged
	}

	public class Controller
	{
		public delegate void EngageSystem();
		public delegate string DisengageSystem();
		public delegate void SetStatus(string status);

		private Heater _heater;
		private Boiler _boiler;

		public Controller()
		{
			this._heater = new Heater();
			this._boiler = new Boiler();
		}

		public void EngageMachinery()
		{
			EngageSystem d;
			d = this._heater.Engage;
			d += this._boiler.Engage;

			d.Invoke();

			Console.WriteLine("Is Boiler Engaged: {0}", this._boiler.IsRunning ? "Yes" : "No");
			Console.WriteLine("Is Heater Engaged: {0}", this._heater.IsRunning ? "Yes" : "No");
		}

		public void DisengageMachinery()
		{
			DisengageSystem d;
			d = this._boiler.Disengate;
			d += this._boiler.Disengate;

			Console.WriteLine("Boiler and Heater are both {0}", d.Invoke());
		}

		public void ApplyStatus(string status)
		{
			SetStatus s;
			s = this._boiler.SetStatus;
			s += this._heater.SetStatus;
			s.Invoke(status);

			Console.WriteLine("Boiler status is: {0}", this._boiler.Status);
			Console.WriteLine("Heater status is: {0}", this._heater.Status);
		}

		public void EngageAndSetStatus(EngageSystem system, SetStatus status)
		{
			system.Invoke();
			status.Invoke(StatusList.Damaged.ToString());

			foreach (var item in system.GetInvocationList())
			{
				var machine = (IMachinery)system.Target;

				Console.WriteLine("{0} has state of: {1}", machine.ToString(), machine.GetState() ? "Running" : "Not Running");
			}

			foreach (var item in status.GetInvocationList())
			{
				var machine = (IMachinery)system.Target;

				Console.WriteLine("{0} has state of: {1}", machine.ToString(), machine.GetStatus());
			}
		}
	}

	public interface IMachinery
	{
		bool GetState();
		string GetStatus();
		void Engage();
		void SetStatus(string status);
	}

	public class Boiler : IMachinery
	{
		public string Status { get; private set; }

		public bool IsRunning { get; private set; }

		public bool GetState()
		{
			return this.IsRunning;
		}

		public string GetStatus()
		{
			return this.Status;
		}
		
		public void Engage()
		{
			this.IsRunning = true;
		}

		public string Disengate()
		{
			this.IsRunning = false;

			return "Disengaged";
		}

		public void SetStatus(string status)
		{
			this.Status = Enum.Parse(typeof(StatusList), status).ToString();
		}

		public override string ToString()
		{
			return "Boiler";
		}
	}

	public class Heater : IMachinery
	{
		public string Status { get; private set; }

		public bool IsRunning { get; private set; }

		public bool GetState()
		{
			return this.IsRunning;
		}

		public string GetStatus()
		{
			return this.Status;
		}

		public void Engage()
		{
			this.IsRunning = true;
		}

		public string Disengate()
		{
			this.IsRunning = false;

			return "Disengaged";
		}

		public void SetStatus(string status)
		{
			this.Status = Enum.Parse(typeof(StatusList), status).ToString();
		}

		public override string ToString()
		{
			return "Boiler";
		}
	}
}
